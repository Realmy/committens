#! /usr/bin/env node
import 'colors';
import arg from 'arg';

import runHelpCommand from './lib/cli/help.js';
import runCredentialsCommand from './lib/cli/credentials.js';
import runUploadCommand from './lib/cli/upload.js';
import runPushCommand from './lib/cli/push.js';

const args = arg({
  '--help': Boolean,
  '--credentials': Boolean,
  '--push': String,
  '--upload': String,
  '-h': '--help',
  '-c': '--credentials',
  '-p': '--push',
  '-u': '--upload',

  '--path': String,
  '--name': String,
}, {
  permissive: true,
});

if (args['--help']) runHelpCommand();
else if (args['--credentials']) await runCredentialsCommand(args);
else if (args['--upload']) await runUploadCommand(args);
else if (args['--push']) await runPushCommand(args);
else {
  console.warn(`No command found, running "${'committens --help'.bold}".`.blue);
  runHelpCommand();
}

process.exit();