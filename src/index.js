import { walk } from './lib/api/walk.js';
import { allowedFileTypes } from './lib/api/allowedFileTypes.js';
import fs from 'node:fs';
import path from 'node:path';

export default class Committens {
  /**
   * @type {Config}
   */
  #apiKey;
  #username;
  #password;

  /**
   * Constructs a new instance of Committens.
   * @param {object} auth - Authentication information.
   * @param {object} [auth.credentials] - Authentication credentials.
   * @param {string} auth.credentials.username - The username for authentication.
   * @param {string} auth.credentials.password - The password for authentication.
   * @param {string} auth.apiKey - The API key for authentication.
   */
  constructor(auth) {
    if (auth.credentials) {
      this.#username = auth.credentials.username;
      this.#password = auth.credentials.password;
    } else if (auth.apiKey) {
      this.#apiKey = auth.apiKey;
    }
  }

  /**
   * Gets/caches the API key from your Neocities account, or returns the cached API key if it exists.
   * @returns {Promise<string>}
   */
  async getApiKey() {
    if (this.#apiKey) return this.#apiKey;

    const resp = await fetch(`https://neocities.org/api/key`, {
      method: 'GET',
      headers: {
        'Authorization': 'Basic ' + Buffer.from(this.#username + ':' + this.#password).toString('base64'),
      },
    });

    if (resp.status === 500) throw new Error('500 Internal Server Error occured when requesting Neocities');
    const json = await resp.json();
    if (json.result && json.result === 'error') throw new Error(json.message);
    this.#apiKey = json.api_key;
    return this.#apiKey;
  }

  /**
   * Uploads an array of file paths to Neocities.
   * @param {Object[]} files - Array of file paths
   * @param {string} files.name - Destination path on Neocities
   * @param {string} files.path - Local path to file
   * @returns {Promise<boolean>}
   */
  async upload(files) {
    const formData = new FormData();
    for (const file of files) formData.append(file.name, new Blob([fs.readFileSync(file.path)]), path.basename(file.path));

    const resp = await fetch(`https://neocities.org/api/upload`, {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${await this.getApiKey()}`,
      },
      body: formData,
    });

    if (resp.status === 500) throw new Error('500 Internal Server Error occured when requesting Neocities');
    const json = await resp.json();
    if (json.result && json.result === 'error') throw new Error(json.message);

    return true;
  }

  /**
   * Recursively searches dist folder for static files, and uploads them to Neocities.
   * @param {string} dist - Path to statically generated site
   * @param {string} remotePath - Destination path on Neocities
   */
  async push(dist, remotePath = '/') {
    const files = await walk(dist);

    for (const file of files) {
      if (!allowedFileTypes.includes(path.extname(file).slice(1))) throw new Error(`File "${path.relative(dist, file).split('\\').join('/')}" has an unsupported file type`);
    }

    await this.upload(files.map(file => ({ name: path.join(remotePath || '/', path.relative(dist, file)).split('\\').join('/'), path: file })));

    return true;
  }
}