import Committens from '../../index.js';
import getCredentials from './misc/getCredentials.js';
import { existsSync } from 'node:fs';
import path from 'node:path';

/**
 * Pushes a directory to Neocities.
 * @param {Object} args - Command arguments
 * @returns {Promise<boolean>}
 */
export default async function runPushCommand(args) {
  const apiKey = await getCredentials();
  const committens = new Committens({ apiKey });

  if (!args['--push'] || !existsSync(args['--push'])) {
    console.log(`The directory you tried to push doesn't exist.`.red.bold);
    return false;
  }

  const remotePath = args['--path'] || '/';

  console.log(`Pushing the files in "${args['--push'].bold}" to Neocities under "${remotePath.bold}"`.cyan);

  try {
    await committens.push(args['--push'], remotePath);
  } catch (err) {
    console.error(`${'Unable to push files'.bold}: ${err.message}`.red);
    return false;
  }

  console.log('Directory successfully pushed! 😸'.green.bold);

  return true;
}