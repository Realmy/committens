/**
 * Logs CLI usage information.
 * @returns {boolean}
 */
export default function runHelpCommand() {
  console.log([
    '',
    `${`Committens`.bold} 🐱🧤 - A little tool to help you manage your Neocities site`.yellow,
    '',
    `${`Usage`.bold}:`,
    '  committens <command> [options...]',
    '',
    `${`Commands`.bold}:`,
    '  -h, --help               Show help',
    '  -c, --credentials        Save project credentials',
    '  -p, --push <directory>   Push a directory to Neocities',
    '  -u, --upload <file>      Upload a file to Neocities',
    '',
    `${`Options`.bold}:`,
    '  --path <path>            Path on Neocities to upload/push files to. Defaults to "/".',
    '  --name <name>            Name of the file on Neocities, in the case of an upload. Defaults to the original filename.',
  ].join('\n'));

  return true;
}