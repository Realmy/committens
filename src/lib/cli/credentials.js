import fs from 'node:fs/promises';
import { existsSync } from 'node:fs';
import path from 'node:path';
import getCredentials from './misc/getCredentials.js';

/**
 * Saves API key in a ".neocreds" file.
 * @returns {Promise<boolean>}
 */
export default async function runCredentialsCommand() {
  console.log('Starting the creation of a .neocreds file'.bold);
  console.log('This file contains an API key, and will be used by committens to automatically authenticate you when interacting with Neocities.');

  // Check if credentials file exists
  if (existsSync(path.join(process.cwd(), '.neocreds'))) {
    console.warn(`[${'Warning'.bold}] You already have an API key (.neocreds) file. This action will overwrite it.`.yellow);
  }

  // Ask for credentials
  const apiKey = await getCredentials(true);

  // Write to file
  await fs.writeFile(path.join(process.cwd(), '.neocreds'), apiKey);

  // Add to gitignore (if it isn't already in it)
  if (existsSync(path.join(process.cwd(), '.gitignore'))) {
    const gitignore = await fs.readFile(path.join(process.cwd(), '.gitignore'), 'utf8');
    if (!gitignore.includes('.neocreds')) await fs.appendFile(path.join(process.cwd(), '.gitignore'), '\n.neocreds\n');
  } else {
    await fs.writeFile(path.join(process.cwd(), '.gitignore'), '.neocreds\n');
  }

  if (!existsSync(path.join(process.cwd(), '.git'))) {
    console.warn(`[${'Note'.bold}] You don't appear to be inside of a git repository, but I added .neocreds to a .gitignore just to be sure. Stay safe!`.blue);
    console.warn(`[${'Note'.bold}] Also, it's generally a good practice to put your .neocreds file in the root of your project. Just so you're aware!`.blue);
  } else {
    console.log(`[${'Note'.bold}] .neocreds was added to a .gitignore.`.blue);
  }

  console.log(`Successfully made a .neocreds file`.green.bold);

  return true;
}