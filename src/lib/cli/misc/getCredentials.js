import fs from 'node:fs/promises';
import path from 'node:path';
import { existsSync } from 'node:fs';
import readline from 'node:readline/promises';
import Committens from '../../../index.js';

/**
 * Attempts to get API key from a .neocreds file, and if it doesn't exist, prompts the user to enter them.
 * @param {boolean} skipFile Whether to look for a .neocreds file.
 * @returns {Promise<string>} The API key.
 */
export default async function getCredentials(skipFile = false) {
  if (!skipFile) {
    if (existsSync(path.join(process.cwd(), '.neocreds'))) {
      const apiKey = (await fs.readFile(path.join(process.cwd(), '.neocreds'), 'utf8')).trim();
      console.log('Using API key from .neocreds file'.blue);
      return apiKey;
    } else {
      console.warn('Could not find .neocreds file, please enter your credentials:'.blue);
    }
  }

  // Ask for credentials
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  const username = (await rl.question(`What is your ${'username'.bold}? > `.cyan)).trim();
  const password = (await rl.question(`What is your ${'password'.bold}? > `.cyan)).trim();
  rl.close();

  const committens = new Committens({ credentials: { username, password } });
  const apiKey = await committens.getApiKey();

  if (!skipFile) console.log(`You can save this API key in your project by running "${'committens --credentials'.bold}", so that you don't have to enter your credentials again.`.blue);

  return apiKey;
}