import Committens from '../../index.js';
import getCredentials from './misc/getCredentials.js';
import { existsSync } from 'node:fs';
import path from 'node:path';

/**
 * Uploads a single file to Neocities.
 * @param {Object} args - Command arguments
 * @returns {Promise<boolean>}
 */
export default async function runUploadCommand(args) {
  const apiKey = await getCredentials();
  const committens = new Committens({ apiKey });

  if (!args['--upload'] || !existsSync(args['--upload'])) {
    console.log(`The file you tried to upload doesn't exist.`.red.bold);
    return false;
  }

  const remotePath = args['--path'] || '/';
  const remoteName = args['--name'] || path.basename(args['--upload']);

  console.log(`Uploading "${path.basename(args['--upload']).bold}" to Neocities as "${path.join(remotePath, remoteName).split('\\').join('/').bold}"`.cyan);

  try {
    await committens.upload([
      {
        name: path.join(remotePath, remoteName).split('\\').join('/'),
        path: args['--upload'],
      },
    ]);
  } catch (err) {
    console.error(`${'Unable to upload file'.bold}: ${err.message}`.red);
    return false;
  }

  console.log('File successfully uploaded! 😸'.green.bold);

  return true;
}