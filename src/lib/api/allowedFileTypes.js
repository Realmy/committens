/**
 * File types that Neocities allows for upload (for non-supporter users), from `https://neocities.org/site_files/allowed_types`.
 */ 
const allowedFileTypes = ['apng', 'asc', 'atom', 'avif', 'bin', 'css', 'csv', 'dae', 'eot', 'epub', 'geojson', 'gif', 'gltf', 'gpg', 'htm', 'html', 'ico', 'jpeg', 'jpg', 'js', 'json', 'key', 'kml', 'knowl', 'less', 'manifest', 'map', 'markdown', 'md', 'mf', 'mid', 'midi', 'mtl', 'obj', 'opml', 'osdx', 'otf', 'pdf', 'pgp', 'pls', 'png', 'rdf', 'resolveHandle', 'rss', 'sass', 'scss', 'svg', 'text', 'toml', 'tsv', 'ttf', 'txt', 'webapp', 'webmanifest', 'webp', 'woff', 'woff2', 'xcf', 'xml', 'yaml', 'yml'];
export { allowedFileTypes };