import fs from 'node:fs/promises';
import path from 'node:path';

/**
 * Recursively walks through a directory and gathers all file paths.
 *
 * @param {string} dir - The directory path to start the walk.
 * @returns {Promise<string[]>} - An array of file paths.
 */
export async function walk(dir = dist) {
  const paths = [];
  const files = await fs.readdir(dir);

  for (const file of files) {
    const filePath = path.join(dir, file);
    const stat = await fs.stat(filePath);

    if (stat.isDirectory()) {
      paths.push(...(await walk(filePath)));
    } else {
      paths.push(filePath);
    }
  }

  return paths;
}