# Committens

A little Node.js-based tool to help you manage your Neocities site. Also (through the developer API) serves as a newer, more modern alternative to the [neocities](https://npmjs.com/package/neocities) npm package.

**Note**: This project is under construction!